/*
 * GRUB  --  GRand Unified Bootloader
 * Copyright (C) 2013  Free Software Foundation, Inc.
 * Copyright (c) 2007 The DragonFly Project.  All rights reserved.
 *
 * This code is derived from software contributed to The DragonFly Project
 * by Matthew Dillon <dillon@backplane.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name of The DragonFly Project nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific, prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $DragonFly: src/sys/sys/disklabel64.h,v 1.4 2007/06/19 06:39:10 dillon Exp $
 */

#ifndef GRUB_DISKLABEL64_PARTITION_HEADER
#define GRUB_DISKLABEL64_PARTITION_HEADER 1

#define GRUB_DISKLABEL64_MAGIC		  ((grub_uint32_t)0xc4464c59)
#define GRUB_DISKLABEL64_MAX_PARTITIONS	  16

/*
 * Note: offsets are relative to the base of the slice, NOT to
 * pbase.  Unlike 32 bit disklabels the on-disk format for
 * a 64 bit disklabel remains slice-relative.
 *
 * An uninitialized partition has a boffset and bsize of 0.
 *
 * If fstype is not supported for a live partition it is set
 * to FS_OTHER.  This is typically the case when the filesystem
 * is identified by its uuid.
 */
struct grub_partition_disklabel64_entry
{
  /* slice relative offset, in bytes */
  grub_uint64_t   boffset;
  /* size of partition, in bytes */
  grub_uint64_t   bsize;
  grub_uint8_t    fstype;
  /* reserved, all must be 0 */
  grub_uint8_t    unused01;
  grub_uint8_t    unused02;
  grub_uint8_t    unused03;
  grub_uint32_t   unused04;
  grub_uint32_t   unused05;
  grub_uint32_t   unused06;
  /* unused by GRUB struct uuid type_uuid */
  grub_uint8_t    unused07[16];
  /* unused by GRUB struct uuid stor_uuid */
  grub_uint8_t    unused08[16];
} __attribute__ ((packed));

/*
 * A disklabel64 starts at slice relative offset 0, NOT SECTOR 1.  In
 * otherwords, magic is at byte offset 512 within the slice, regardless
 * of the sector size.
 *
 * The reserved0 area is not included in the crc and any kernel writeback
 * of the label will not change the reserved area on-disk.  It is purely
 * a shim to allow us to avoid sector calculations when reading or
 * writing the label.  Since byte offsets are used in our 64 bit disklabel,
 * the entire disklabel and the I/O required to access it becomes
 * sector-agnostic.
 */
struct grub_partition_disklabel64
{
  /* reserved or unused */
  grub_int8_t     reserved0[512];
  /* the magic number */
  grub_uint32_t   magic;
  /* crc32() magic thru last part */
  grub_uint32_t   crc;
  /* partition alignment requirement */
  grub_uint32_t   align;
  /* number of partitions */
  grub_uint32_t   npartitions;
  /* unused by GRUB struct uuid stor_uuid */
  grub_uint8_t    unused01[16];

  /* total size incl everything (bytes) */
  grub_uint64_t   total_size;
  /* boot area base offset (bytes) */
  /* boot area is pbase - bbase */
  grub_uint64_t   bbase;

  /* first allocatable offset (bytes) */
  grub_uint64_t   pbase;
  /* last allocatable offset+1 (bytes) */
  grub_uint64_t   pstop;
  /* location of backup copy if not 0 */
  grub_uint64_t   abase;

  grub_uint8_t    packname[64];
  grub_uint8_t    reserved[64];

  /*
   * Partition table entries follow. In original struct definition
   * they were declared as part of this structure like:
   * struct grub_partition_disklabel64_entry
   *	partitions[GRUB_DISKLABEL64_MAX_PARTITIONS];
   */
} __attribute__ ((packed));

#endif /* ! GRUB_DISKLABEL64_PARTITION_HEADER */
